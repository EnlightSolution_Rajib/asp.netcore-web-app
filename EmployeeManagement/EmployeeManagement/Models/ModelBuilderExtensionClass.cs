﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Models
{
    public static class ModelBuilderExtensionClass
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>().HasData(
                new Employee()
                {
                    Id = 4,
                    Name = "Mark",
                    Email = "mark@gmail.com",
                    Department = Dept.EEE
                },
                new Employee()
                {
                    Id = 5,
                    Name = "Rock",
                    Email = "rock@gmail.com",
                    Department = Dept.BBE
                }
            );
        }
    }
}
