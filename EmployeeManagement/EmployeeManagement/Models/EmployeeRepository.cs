﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace EmployeeManagement.Models
{
    public class EmployeeRepository: IEmployeeRepository
    {
        private readonly AppDbContext _db;
        private List<Employee> _employees;
        public EmployeeRepository(AppDbContext db)
        {
            _db = db;
            //_employees = new List<Employee>()
            //{
            //    new Employee() {Id = 1, Name = "Rajib Sarker", Email = "RajibSarker320@gmail.com", Department = Dept.CSE},
            //    new Employee() {Id = 2, Name = "Raju Sarker", Email = "RajuSarker320@gmail.com", Department = Dept.Mechanical},
            //    new Employee() {Id = 3, Name = "Saju Sarker", Email = "SajuSarker320@gmail.com", Department = Dept.EEE},
            //    //new Employee() {Id = 4, Name = "Sadhan Sarker", Email = "SadhanSarker320@gmail.com", Department = "BBE"}
            //};
        }

        public Employee GetEmployeeById(int id)
        {
            //return _employees.FirstOrDefault(c => c.Id == id);
            return _db.Employees.FirstOrDefault(c => c.Id == id);
        }

        public IEnumerable<Employee> GetEmployees()
        {
            return _db.Employees.ToList();
        }

        public Employee Add(Employee employee)
        {
            //employee.Id = _employees.Max(c => c.Id) + 1;
            //_employees.Add(employee);

            _db.Employees.Add(employee);
            _db.SaveChanges();
            return employee;
        }

        public Employee Update(Employee employee)
        {
            var emp = _db.Employees.Attach(employee);
            emp.State = EntityState.Modified;
            _db.SaveChanges();
            return employee;
        }

        public Employee Delete(int id)
        {
            var employee = _db.Employees.FirstOrDefault(c => c.Id == id);
            if (employee != null)
            {
                _db.Employees.Remove(employee);
                _db.SaveChanges();
            }

            return employee;
        }
    }
}
