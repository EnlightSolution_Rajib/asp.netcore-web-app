﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Models;
using EmployeeManagement.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace EmployeeManagement.Controllers
{
    // attribute routeing
    //[Route("[controller]/[action]")]
    public class HomeController : Controller
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public HomeController(IEmployeeRepository employeeRepository, IHostingEnvironment hostingEnvironment)
        {
            _employeeRepository = employeeRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        //[Route("/Home")]
        //[Route("~/")]
        //[Route("")]
        //[Route("Home/Index")]
        public ViewResult Index()
        {
            return View(_employeeRepository.GetEmployees());
        }

        public ViewResult Details(int id)
        {
            throw new Exception("Exception to see employee details.");
            var employee = _employeeRepository.GetEmployeeById(id);
            if(employee == null)
            {
                Response.StatusCode = 404;
                return View("EmployeeNotFound", id);
            }
            HomeDetailsViewModel model = new HomeDetailsViewModel()
            {
                Employee = employee,
                PageTitle = "Employee Details"
            };

            //ViewData["Employee"] = employee;
            //ViewData["PageTitle"] = "Employee Details";
            //ViewBag.Employee = employee;
            return View(model);
        }

        public ViewResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(EmployeeCreateViewModel model)
        {
            if (ModelState.IsValid)
            {
                string fileName = ProcessUploadedFile(model);

                Employee employee = new Employee()
                {
                    Name = model.Name,
                    Email = model.Email,
                    Department = model.Department,
                    PhotoPath = fileName
                };

                var employeeObj = _employeeRepository.Add(employee);
                return RedirectToAction("details", new { id = employeeObj.Id });
            }
            return View();
        }


        public ViewResult Edit(int id)
        {
            var existingEmployee = _employeeRepository.GetEmployeeById(id);
            if (existingEmployee == null)
            {
                Response.StatusCode = 404;
                return View("EmployeeNotFound", id);
            }
            EmployeeEditViewModel employee = new EmployeeEditViewModel()
            {
                Id = existingEmployee.Id,
                Name = existingEmployee.Name,
                Department = existingEmployee.Department,
                Email = existingEmployee.Email,
                ExistingPhotoPath = existingEmployee.PhotoPath
            };
            return View(employee);
        }

        [HttpPost]
        public IActionResult Edit(EmployeeEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                var existingEmployee = _employeeRepository.GetEmployeeById(model.Id);
                
                if (existingEmployee != null)
                {
                    existingEmployee.Name = model.Name;
                    existingEmployee.Email = model.Email;
                    existingEmployee.Department = model.Department;
                    if (model.Photo != null)
                    {
                        if (model.ExistingPhotoPath != null)
                        {
                            // deleting the exiting image
                            var filePath = Path.Combine(_hostingEnvironment.WebRootPath, "images",
                                model.ExistingPhotoPath);
                            System.IO.File.Delete(filePath);
                        }
                        existingEmployee.PhotoPath = ProcessUploadedFile(model);
                    }
                    
                    var employeeObj = _employeeRepository.Update(existingEmployee);
                    return RedirectToAction("Index");
                }

                return View();
            }
            return View();
        }

        private string ProcessUploadedFile(EmployeeCreateViewModel model)
        {
            string fileName = "";
            if (model.Photo != null)
            {
                var uploadedFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images");
                fileName = Guid.NewGuid().ToString() + "_" + model.Photo.FileName;
                var filePath = Path.Combine(uploadedFolder, fileName);
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    model.Photo.CopyTo(stream);
                }
            }

            return fileName;
        }
    }
}
