using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace EmployeeManagement
{
    public class Startup
    {
        public IConfiguration _configuration { get; }
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)  // dependency injection container
        {
            services.AddDbContextPool<AppDbContext>(option =>
                option.UseNpgsql(_configuration.GetConnectionString("DbConnection")));

            services.AddMvc().AddXmlSerializerFormatters();
            services.AddTransient<IEmployeeRepository, EmployeeRepository>();

            //services.AddMvcCore().AddJsonFormatters(j => j.Formatting = Formatting.Indented);

            //services.Configure<CookiePolicyOptions>(options =>
            //{
            //    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
            //    options.CheckConsentNeeded = context => true;
            //    options.MinimumSameSitePolicy = SameSiteMode.None;
            //});


            //services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILogger<Startup> logger)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                //app.UseStatusCodePages();
                //app.UseStatusCodePagesWithRedirects("/Error/{0}");

                app.UseExceptionHandler("/Error");
            }

            //app.UseStaticFiles();
            //app.UseCookiePolicy();

            //app.UseMvc();


            //FileServerOptions fileServerOptions = new FileServerOptions();
            //fileServerOptions.DefaultFilesOptions.DefaultFileNames.Clear();
            //fileServerOptions.DefaultFilesOptions.DefaultFileNames.Add("foo.html");

            //app.UseFileServer(fileServerOptions);

            ////practice for how to use default files
            //DefaultFilesOptions defaultFilesOptions = new DefaultFilesOptions();
            //defaultFilesOptions.DefaultFileNames.Clear();
            //defaultFilesOptions.DefaultFileNames.Add("foo.html");
            //app.UseDefaultFiles(defaultFilesOptions);

            app.UseStaticFiles();
            app.UseMvcWithDefaultRoute();
            app.UseMvc(route =>
            {
                // conventional routing
                route.MapRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            //// practice for middleware concept
            //app.Use(async (context, next) =>
            //{
            //    logger.LogInformation("Request accept to first middleware");
            //    await next();
            //    logger.LogInformation("Response going from first middleware");
            //});
            //app.Use(async (context, next) =>
            //{
            //    logger.LogInformation("Request accept to second middleware");
            //    await next();
            //    logger.LogInformation("Response going from second middleware");
            //});


            //// this app.Run() is a terminal middleware, that does not called the next middleware.
            //app.Run(async (context) => { await context.Response.WriteAsync(System.Diagnostics.Process.GetCurrentProcess().ProcessName); });

            //app.Run(async (context) =>
            //{
            //    logger.LogInformation("Request accept to third middleware");
            //    await context.Response.WriteAsync("Hello World2!");
            //    logger.LogInformation("Response going from third middleware");
            //});
        }
    }
}
